# Sea Server - An educational Java HTTP server

Sea Server is a barebones HTTP server written in Java. I created it to understand the HTTP protocol and the nature of the work done by real HTTP servers like Jetty and Tomcat.

Sea Server is an educational project. It's designed to be small and embeddable.

## Getting Started

Pull the code and build & run with maven.

```
git clone https://chrispaola@bitbucket.org/chrispaola/seaserver-core.git
cd seaserver-core
mvn install
mvn exec:java
```

With the server running, try out the URLs below in your browser.

| URL | Description |
| ------ | ------ |
| http://localhost:8086/ | HTML document that embeds a motion-JPEG image |
| http://localhost:8086/echo | GET/POST form example |
| http://localhost:8086/image | Dynamic image rendering |
| http://localhost:8086/echo | Dynamic motion-JPEG rendering  |


## Source Code

To embed in an application, start the server with your own handlers, like so.

```java
    public static void main(String[] args) throws InterruptedException {

        SeaServer server = new SeaServer(8086, 8);

        server.addHandler("/", new IndexHandler());
        server.addHandler("/echo", new EchoHandler());
        server.addHandler("/image", new ImageHandler());
        server.addHandler("/motion", new MotionJpegHandler());

        server.start();
        server.join();
    }
```

Handler is an abstract class responsible for processing an HTTP request and returning a response.

```java
public abstract class Handler {

    public void handlePost(Request request, Response response) throws IOException {
    }

    public void handleGet(Request request, Response response) throws IOException {
    }

    public void handlePut(Request request, Response response) throws IOException {
    }

    public void handleDelete(Request request, Response response) throws IOException {
    }
}
```

### Form Input Example

This example shows form input.

The browser first sends a GET request, an HTML document containing a form is returned.

The form is posted by the browser via a POST request; the request body carries key/value pairs contained within the form.

```java
 public class EchoHandler extends Handler {

     @Override
     public void handleGet(Request request, Response response) throws IOException {

         response.setContentType("text/html");
         OutputStream outputStream = response.getOutputStream();

         String form = "<form name=\"input\" action=\"/echo\" method=\"post\">" + //
                 "Username: <input type=\"text\" name=\"user\">" + //
                 "<input type=\"submit\" value=\"Submit\">" + //
                 "</form>";

        outputHeader(response);

        IOUtils.write("<h1>Form</h1>" + form, outputStream);

        outputFooter(response);
     }

     @Override
     public void handlePost(Request request, Response response) throws IOException {

         outputHeader(response);

         int length = request.getContentLength();
         byte[] bytes = new byte[length];
         request.getInputStream().read(bytes);
         String str = new String(bytes, "UTF-8");
         IOUtils.write(str, response.getOutputStream());

         outputFooter(response);
     }

     private void outputHeader(Response response) throws IOException {
         IOUtils.write("<html><title>foo</title><body>", response.getOutputStream());
     }

     private void outputFooter(Response response) throws IOException {
         IOUtils.write("</body></html>", response.getOutputStream());
     }
 }
```

![Screenshot of form example](doc/echo-example.gif)

### Dynamic JPEG Rendering

This example demonstrates dynamically rendering a JPEG image.

```java
public class ImageHandler extends Handler {

    @Override
    public void handleGet(Request request, Response response) throws IOException {
        response.setContentType("image/jpeg");

        // Create image
        int width = 200, height = 200;
        BufferedImage image = new BufferedImage(width, height,
                BufferedImage.TYPE_INT_RGB);

        // Get drawing context
        Graphics2D g2d = image.createGraphics();

        // Fill background with white
        g2d.setColor(Color.WHITE);
        g2d.fillRect(0, 0, width, height);

        // Draw a smiley face
        g2d.setColor(Color.YELLOW);
        g2d.fillOval(10, 10, 180, 180);
        g2d.setColor(Color.BLACK);
        g2d.fillOval(40, 40, 40, 40);
        g2d.fillOval(120, 40, 40, 40);
        g2d.fillRect(50, 150, 100, 10);

        // Dispose context
        g2d.dispose();

        // Write image to the output stream
        OutputStream os = response.getOutputStream();

        ImageIO.write(image, "jpeg", os);
    }
}
```

![Screenshot of dynamic jpeg rendering example](doc/jpeg-example.gif)

### Motion JPEG Rendering

This example uses the [Mixed Replace](https://en.wikipedia.org/wiki/MIME#Mixed-Replace) feature of HTTP to dynamically update a JPEG image in-place, in the browser.

Note that this is closely related to, but not the same as [Motion JPEG](https://en.wikipedia.org/wiki/Motion_JPEG).

```java
public class MotionJpegHandler extends Handler {

    @Override
    public void handleGet(Request request, Response response) throws IOException {

        response.setContentType("multipart/x-mixed-replace; boundary=--boundary");

        int width = 200, height = 200;
        BufferedImage image = new BufferedImage(width, height,
                BufferedImage.TYPE_INT_RGB);

        // Get drawing context
        Graphics2D g2d = image.createGraphics();

        for (int i = 0; true; i++) {

            // Fill background with white
            g2d.setColor((i % 2) == 1 ? Color.BLACK : Color.BLUE);
            g2d.fillRect(0, 0, width, height);

			// Draw a smiley face
            g2d.setColor(Color.YELLOW);
            g2d.fillOval(10, 10, 180, 180);
            g2d.setColor((i % 2) == 0 ? Color.BLACK : Color.BLUE);
            g2d.fillOval(40, 40, 40, 40);
            g2d.fillOval(120, 40, 40, 40);
            g2d.fillRect(50, 150, 100, 10);

            // Write image to the output stream
            OutputStream os = response.getOutputStream();

            ByteArrayOutputStream tmp = new ByteArrayOutputStream();
            ImageIO.write(image, "jpeg", tmp);
            tmp.close();
            int contentLength = tmp.size();

            IOUtils.write("--boundary\r\n", os);
            IOUtils.write("Content-Type: image/jpeg\r\n", os);
            IOUtils.write("Content-length: " + contentLength + "\r\n", os);
            IOUtils.write("\r\n", os);

            os.write(tmp.toByteArray());

            IOUtils.write("\r\n", os);
            IOUtils.write("\r\n", os);
        }
    }
}
```

![Screenshot of dynamic jpeg rendering example](doc/motion-jpeg-example.gif)

