/**
 Copyright 2012 Chris Paola

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package com.chrisp.seaserver.handler;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.imageio.ImageIO;

import com.chrisp.seaserver.core.Handler;
import com.chrisp.seaserver.core.Request;
import com.chrisp.seaserver.core.Response;
import org.apache.commons.io.IOUtils;

public class MotionJpegHandler extends Handler {

    @Override
    public void handleGet(Request request, Response response) throws IOException {

        response.setContentType("multipart/x-mixed-replace; boundary=--boundary");

        int width = 200, height = 200;
        BufferedImage image = new BufferedImage(width, height,
                BufferedImage.TYPE_INT_RGB);

        // Get drawing context
        Graphics2D g2d = image.createGraphics();

        for (int i = 0; true; i++) {

            // Fill background with white
            g2d.setColor((i % 2) == 1 ? Color.BLACK : Color.BLUE);
            g2d.fillRect(0, 0, width, height);

			// Draw a smiley face
            g2d.setColor(Color.YELLOW);
            g2d.fillOval(10, 10, 180, 180);
            g2d.setColor((i % 2) == 0 ? Color.BLACK : Color.BLUE);
            g2d.fillOval(40, 40, 40, 40);
            g2d.fillOval(120, 40, 40, 40);
            g2d.fillRect(50, 150, 100, 10);

            // Write image to the output stream
            OutputStream os = response.getOutputStream();

            ByteArrayOutputStream tmp = new ByteArrayOutputStream();
            ImageIO.write(image, "jpeg", tmp);
            tmp.close();
            int contentLength = tmp.size();

            IOUtils.write("--boundary\r\n", os);
            IOUtils.write("Content-Type: image/jpeg\r\n", os);
            IOUtils.write("Content-length: " + contentLength + "\r\n", os);
            IOUtils.write("\r\n", os);

            os.write(tmp.toByteArray());

            IOUtils.write("\r\n", os);
            IOUtils.write("\r\n", os);
        }
    }
}