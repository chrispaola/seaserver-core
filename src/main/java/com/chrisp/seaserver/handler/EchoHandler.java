/**
 Copyright 2012 Chris Paola

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package com.chrisp.seaserver.handler;

import com.chrisp.seaserver.core.Handler;
import com.chrisp.seaserver.core.Request;
import com.chrisp.seaserver.core.Response;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.OutputStream;

@SuppressWarnings("serial")
public class EchoHandler extends Handler {

    @Override
    public void handleGet(Request request, Response response) throws IOException {

        response.setContentType("text/html");
        OutputStream outputStream = response.getOutputStream();

        String form = "<form name=\"input\" action=\"/echo\" method=\"post\">" + //
                "Username: <input type=\"text\" name=\"user\">" + //
                "<input type=\"submit\" value=\"Submit\">" + //
                "</form>";

        outputHeader(response);

        IOUtils.write("<h1>Form</h1>" + form, outputStream);

        outputFooter(response);
    }

    @Override
    public void handlePost(Request request, Response response) throws IOException {

        outputHeader(response);

        int length = request.getContentLength();
        byte[] bytes = new byte[length];
        request.getInputStream().read(bytes);
        String str = new String(bytes, "UTF-8");
        IOUtils.write(str, response.getOutputStream());

        outputFooter(response);
    }

    private void outputHeader(Response response) throws IOException {
        IOUtils.write("<html><title>foo</title><body>", response.getOutputStream());
    }

    private void outputFooter(Response response) throws IOException {
        IOUtils.write("</body></html>", response.getOutputStream());
    }
}
