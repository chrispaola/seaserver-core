/**
 Copyright 2012 Chris Paola

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package com.chrisp.seaserver.handler;

import com.chrisp.seaserver.core.Handler;
import com.chrisp.seaserver.core.Request;
import com.chrisp.seaserver.core.Response;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;

public class ImageHandler extends Handler {

    @Override
    public void handleGet(Request request, Response response) throws IOException {
        response.setContentType("image/jpeg");

        // Create image
        int width = 200, height = 200;
        BufferedImage image = new BufferedImage(width, height,
                BufferedImage.TYPE_INT_RGB);

        // Get drawing context
        Graphics2D g2d = image.createGraphics();

        // Fill background with white
        g2d.setColor(Color.WHITE);
        g2d.fillRect(0, 0, width, height);

        // Draw a smiley face
        g2d.setColor(Color.YELLOW);
        g2d.fillOval(10, 10, 180, 180);
        g2d.setColor(Color.BLACK);
        g2d.fillOval(40, 40, 40, 40);
        g2d.fillOval(120, 40, 40, 40);
        g2d.fillRect(50, 150, 100, 10);

        // Dispose context
        g2d.dispose();

        // Write image to the output stream
        OutputStream os = response.getOutputStream();

        ImageIO.write(image, "jpeg", os);
    }
}