/**
 Copyright 2012 Chris Paola

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package com.chrisp.seaserver.core;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import com.chrisp.seaserver.core.log.Logger;
import com.chrisp.seaserver.core.log.StdOutLogger;

/**
 * An dead simple http server.
 *
 * Usage:
 * <code>
 *   SeaServer server = new SeaServer(8086, 8);
 *   server.addHandler("/hello", new HelloHandler());
 *   server.start();
 *   server.join();
 * </code>
 *
 * @author chrisp
 */
public class SeaServer extends Thread {
    private static final String TAG = "SeaServer";

    private ServerSocket serverSocket = null;
    private Executor executor = Executors.newCachedThreadPool();
    private HandlerRegistry handlerRegistry = new HandlerRegistry();
    private Logger logger = new StdOutLogger();
    private int port;
    private int backlog;
    private volatile boolean stopped = false;

    public SeaServer(int port, int backlog) {
        this.port = port;
        this.backlog = backlog;
    }

    public void addHandler(String regex, Handler handler) {
        handlerRegistry.addHandler(regex, handler);
    }

    public void removeHandler(String regex) {
        handlerRegistry.removeHandler(regex);
    }

    public void run() {
        try {
            logger.i(TAG, "Starting...");
            serverSocket = new ServerSocket(port, backlog);

            InetAddress address = serverSocket.getInetAddress();
            logger.i(TAG, "%s accepting requests on %s:%s", address.getHostName(), address.getHostAddress(), port);

            while (!stopped) {
                Socket socket = serverSocket.accept();
                RequestProcessor processor = new RequestProcessor(socket, handlerRegistry);
                processor.setLogger(logger);
                executor.execute(processor);
            }
        } catch (IOException e) {
            logger.w(TAG, "Caught exception %s", e.getMessage());
        } finally {
            if (serverSocket != null) {
                try {
                    logger.i(TAG, "Shutting down server socket");
                    serverSocket.close();
                } catch (IOException e) {
                    logger.w(TAG, "Caught exception while closing socket: %s", e);
                }
            }
        }
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }
}
