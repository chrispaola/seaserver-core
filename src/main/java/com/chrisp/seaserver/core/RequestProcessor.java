/**
    Copyright 2012 Chris Paola

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
package com.chrisp.seaserver.core;

import com.chrisp.seaserver.core.log.Logger;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;

/**
 * Parses and dispatchers http requests.
 *
 * @author chrisp
 */
public class RequestProcessor implements Runnable {

    private static final String TAG = "RequestProcessor";

    @SuppressWarnings("serial")
    private static final Set<String> sLegalMethods = new HashSet<String>() {
        {
            add("GET");
            add("POST");
            add("PUT");
            add("DELETE");
        }
    };

    private Socket socket;
    private HandlerRegistry servletRegistry;
    private Logger logger;

    public RequestProcessor(Socket socket, HandlerRegistry servletRegistry) {
        this.socket = socket;
        this.servletRegistry = servletRegistry;
    }

    public void run() {

        InputStream inputStream = null;
        OutputStream outputStream = null;

        try {
            inputStream = socket.getInputStream();
            outputStream = socket.getOutputStream();

            Request request = parseRequest(inputStream);
            Response response = new Response(outputStream);
            try {

                dispatchRequest(request, response);

            } catch (Throwable e) {
                if (!response.isCommitted()) {
                    response.sendError(Response.SC_INTERNAL_SERVER_ERROR);
                }
            }
        } catch (IOException e) {
            // log me
        } finally {
            IOUtils.closeQuietly(inputStream);
            IOUtils.closeQuietly(outputStream);
        }
    }

    private Request parseRequest(InputStream inputStream)
            throws IOException {
        Request request = new Request(inputStream);
        InputStream is = request.getInputStream();

        String line = nextLine(is);

        parseInitialLine(request, line);

        for (line = nextLine(is); StringUtils.isNotBlank(line); line = nextLine(is)) {
            String parts[] = line.split(":");
            if (parts.length < 2) {
                throw new IllegalArgumentException("Bad header: " + line);
            }
            String name = parts[0];
            String value = parts[1];
            request.addHeader(name.trim(), value.trim());
        }

        return request;
    }

    private String nextLine(InputStream is) throws IOException {
        byte[] buffer = new byte[1024];
        StringBuilder builder = new StringBuilder(1024);

        readLine(is, buffer, 0, buffer.length);

        for (int i = 0; i < buffer.length; i++) {
            if (buffer[i] == '\r' || buffer[i] == '\n') {
                break;
            }
            builder.append((char) buffer[i]);
        }

        return builder.toString();
    }

    private void parseInitialLine(Request request, String line) {
        String parts[] = line.split(" ");
        if (parts.length < 2) {
            throw new IllegalArgumentException("Bad inital line " + line);
        }
        String method = parts[0];
        if (!sLegalMethods.contains(method)) {
            throw new IllegalArgumentException("Bad method: " + method);
        }

        request.setMethod(method);

        int index = parts[1].indexOf("?");
        String contextPath = parts[1];
        if (index >= 0) {
            String queryString = parts[1].substring(index + 1); // skip over ?
            request.setQueryString(queryString);
            contextPath = parts[1].trim().substring(0, index);
        }

        request.setContextPath(contextPath);
    }

    private void dispatchRequest(Request request, Response response) {
        try {
            Handler handler = servletRegistry.handlerForPath(request.getPathInfo());

            logger.d(TAG, "Dispatching request for %s to %s", request.getPathInfo(),
                    handler == null ? "(not found)" : handler.getClass().getSimpleName());

            if (handler != null) {
                String method = request.getMethod();
                if ("POST".equals(method)) {
                    handler.handlePost(request, response);
                } else if ("GET".equals(method)) {
                    handler.handleGet(request, response);
                } else if ("PUT".equals(method)) {
                    handler.handlePut(request, response);
                } else if ("DELETE".equals(method)) {
                    handler.handleDelete(request, response);
                } else {
                    response.sendError(Response.SC_NOT_FOUND);
                }
            }
        } catch (IOException e) {
            // empty response - not much we can do
            logger.e(TAG, "Dispatch error %s", e);
        }
    }

    public static int readLine(InputStream is, byte[] b, int off, int len) throws IOException {

        if (len <= 0) {
            return 0;
        }
        int count = 0, c;

        while ((c = is.read()) != -1) {
            b[off++] = (byte) c;
            count++;
            if (c == '\n' || count == len) {
                break;
            }
        }
        return count > 0 ? count : -1;
    }


    public void setLogger(Logger logger) {
        this.logger = logger;
    }
}
