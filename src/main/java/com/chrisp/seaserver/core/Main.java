/**
 Copyright 2012 Chris Paola

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package com.chrisp.seaserver.core;

import java.io.IOException;
import java.io.OutputStream;

import com.chrisp.seaserver.handler.EchoHandler;
import com.chrisp.seaserver.handler.ImageHandler;
import com.chrisp.seaserver.handler.MotionJpegHandler;
import org.apache.commons.io.IOUtils;

/**
 * Main demo entry point.
 */
public class Main {

    public static void main(String[] args) throws InterruptedException {

        SeaServer server = new SeaServer(8086, 8);

        server.addHandler("/", new IndexHandler());
        server.addHandler("/echo", new EchoHandler());
        server.addHandler("/image", new ImageHandler());
        server.addHandler("/motion", new MotionJpegHandler());

        server.start();
        server.join();
    }

    /**
     * Returns an HTML document containing a motion JPEG image.
     */
    private static class IndexHandler extends Handler {

        public void handleGet(Request request, Response response) throws IOException {
            response.setContentType("text/html");
            OutputStream outputStream = response.getOutputStream();
            IOUtils.write("<html><title>foo</title><body><h1>Hello world</h1><img src=/motion></body></html>", outputStream);
        }
    }
}