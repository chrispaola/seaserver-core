/**
 Copyright 2012 Chris Paola

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package com.chrisp.seaserver.core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 * Represents an http request.
 *
 * @author chrisp
 */
public class Request {
    private String method;
    private String contextPath;
    private String queryString;
    private Map<String, String> headers = new HashMap<String, String>();
    private InputStream inputStream;
    private BufferedReader bufferedReader;

    public Request(InputStream inputStream) {
        this.inputStream = inputStream;
        this.bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getContextPath() {
        return contextPath;
    }

    public String getPathInfo() {
        return contextPath;
    }

    public void setContextPath(String contextPath) {
        this.contextPath = contextPath;
    }

    public String getQueryString() {
        return queryString;
    }

    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }

    public String getHeader(String name) {
        return headers.get(name);
    }

    public void addHeader(String name, String value) {
        headers.put(name, value);
    }

    public InputStream getInputStream() throws IOException {
        return inputStream;
    }

    public BufferedReader getReader() throws IOException {
        return bufferedReader;
    }

    public int getContentLength() {
        String str = getHeader("Content-Length");
        return str != null ? Integer.parseInt(str) : -1;
    }
}
