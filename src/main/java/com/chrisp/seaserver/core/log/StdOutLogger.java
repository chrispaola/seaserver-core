/**
 Copyright 2012 Chris Paola

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package com.chrisp.seaserver.core.log;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

public class StdOutLogger implements Logger {

    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy MMM dd, HH:mm:ss:SS");

    @Override
    public void v(String tag, String format, Object... args) {
        log("VERBOSE", tag, format, args);
    }

    @Override
    public void d(String tag, String format, Object... args) {
        log("DEBUG", tag, format, args);
    }

    @Override
    public void i(String tag, String format, Object... args) {
        log("INFO", tag, format, args);
    }

    @Override
    public void w(String tag, String format, Object... args) {
        log("WARN", tag, format, args);
    }

    @Override
    public void e(String tag, String format, Object... args) {
        log("ERROR", tag, format, args);
    }

    private void log(String level, String tag, String format, Object... args) {
        String date = dateFormat.format(new Date());
        String logLine = String.format("[%s] %s %s ", level, date, StringUtils.abbreviate(tag, 16)) + String.format(format, args);
        System.out.println(logLine);
    }
}
